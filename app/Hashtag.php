<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    protected $fillable = [
        'name',
        'active',
        'count',
        'pct',
        'num_likes',
        'num_comments'
    ];

    public function igposts()
    {
        return $this->belongsToMany('App\Igpost');
    }

    public function otherHashtags()
    {
        return $this->belongsToMany('App\Hashtag', 'hashtag_hashtag', 'hashtag_id', 'target_hashtag_id')->withPivot('frequency');
    }

    public function getHashtagByName(String $name)
    {
    	return $this->where('name', $name)->first() ?? abort(404);
    }
}

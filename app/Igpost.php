<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Igpost extends Model
{
    protected $fillable = [
        "ig_hash",
        'link',
        'image',
        'description',
        'user_name',
        'num_likes',
        'num_comments'
    ];

    protected $casts = [
        'post_date' => 'datetime',
        'raw_image' => 'getImage'
	];

	public function hashtags()
    {
        return $this->belongsToMany('App\Hashtag');
    }

    public function getImage($size = "t")
    {
    	// sized [t]humbnail, [m]edium, [l]arge
    	return $this->link . "/media/?size=" . $size;
    }
}

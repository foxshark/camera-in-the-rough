<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
    	return view('templates.about.us');
    }

    public function resources()
    {
    	return view('templates.about.resources');
    }

    public function legal()
    {
    	return view('templates.about.legal');
    }
}

<?php

namespace App\Http\Controllers;

use App\Hashtag;
use App\Igpost;
use Illuminate\Http\Request;

class IgpostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Igpost  $igpost
     * @return \Illuminate\Http\Response
     */
    public function show(Igpost $igpost)
    {
        // $hashtagModel = new Hashtag;
        $hashtags = $igpost->hashtags()->get();

        return view('templates.igposts.detail', ['igpost'=>$igpost, 'hashtags'=>$hashtags]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Igpost  $igpost
     * @return \Illuminate\Http\Response
     */
    public function edit(Igpost $igpost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Igpost  $igpost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Igpost $igpost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Igpost  $igpost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Igpost $igpost)
    {
        //
    }
}

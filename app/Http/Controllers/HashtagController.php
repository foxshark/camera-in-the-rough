<?php

namespace App\Http\Controllers;

use App\Hashtag;
use App\Igpost;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;

class HashtagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $hashtags = Hashtag::orderBy('num_likes', 'desc')->get();

        return view('templates.hashtags.list', ['hashtags'=>$hashtags]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hashtag  $hashtag
     * @return \Illuminate\Http\Response
     */
    public function show(String $hashtagName)
    {
        //primary tag
        $hashtags = new Hashtag;
        $hashtag = $hashtags->getHashtagByName("#".$hashtagName);

        //associated tags
        $associatedTags = $hashtag->otherHashtags()->orderBy('frequency', 'DESC')->limit(20)->get();

        //igposts
        $igposts = $hashtag->igposts()->where("valid", 1)->orderBy('num_likes')->paginate(12);

        $this->validateIgposts($igposts);
        
        return view('templates.hashtags.detail', ['hashtag'=>$hashtag, 'igposts'=>$igposts, 'associatedTags'=>$associatedTags]);
    }

    private function validateIgposts(&$igposts)
    {
        $responses = [];
        $date = new Carbon;
        $date->subWeek();
        foreach ($igposts as $igpost) {
            if($date > $igpost->updated_at) {
                if( !@fopen ($igpost->link . "/media/?size=t", "r") )
                {
                    $igpost->valid = false; 
                    $igpost->save();
                } else {
                    $igpost->touch();
                }
            }
        }

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hashtag  $hashtag
     * @return \Illuminate\Http\Response
     */
    public function edit(Hashtag $hashtag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hashtag  $hashtag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hashtag $hashtag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hashtag  $hashtag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hashtag $hashtag)
    {
        //
    }
}

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="blog-masthead">
          <div class="container">
            <nav class="blog-nav">
              <a class="blog-nav-item active" href="/">Home</a>
              <a class="blog-nav-item" href="/hashtags">Hashtags</a>
              <a class="blog-nav-item" href="/about">About</a>
            </nav>
          </div>
        </div>

        <div class="container">

          <div class="blog-header">
            <h1 class="blog-title">Cameras in the rough</h1>
            <p class="lead blog-description">Aggregating photographic trends and gear</p>
          </div>
            @yield('content')
        
        <footer class="blog-footer">
            <p>By using our website, you hereby consent to our disclaimer and <a href="/about/legal">agree to its terms</a>.</p>
            <p>
                <a href="#">Back to top</a>
            </p>
        </footer>
        </div>
    </body>
</html>

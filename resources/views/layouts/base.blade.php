<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>
        <link rel="stylesheet" href="/css/base.css">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7430268-5"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-7430268-5');
        </script>


    </head>
    <body>
        <header>
            <div class="home-menu pure-menu pure-menu-horizontal xpure-menu-fixed">
                <a class="pure-menu-heading" href="/">Camera in the rough</a>

                <ul class="pure-menu-list">
                    <li class="pure-menu-item"><a href="/hashtags" class="pure-menu-link">Hashtags</a></li>
                    <li class="pure-menu-item"><a href="/about/resources" class="pure-menu-link">Resources</a></li>
                    <li class="pure-menu-item"><a href="/about" class="pure-menu-link">About</a></li>
                </ul>
                <div class="pure-g">
                    <div class="pure-u-1 subhead">Aggregating photographic trends and gear</div>
                </div>
            </div>
        </header>
        
        <main id="app">
            <div class="pure-g">
                @yield('content')
            </div>
        </main>
        
        <footer>
            <div class="legal">
                By using our website, you hereby consent to our disclaimer and <a href="/about/legal">agree to its terms</a>.
            </div>
        </footer>
    </body>
</html>

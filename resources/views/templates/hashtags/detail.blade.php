@extends('layouts.base')

@section('title', 'All Hashtags')


@section('content')
 	<div class="pure-u-1-1 pure-u-sm-1-2">
		<h1>{{$hashtag->name}}</h1>
		Relation to the dataset:
		<ul>
			<li>Count: {{number_format($hashtag->count)}}</li>
			<li>Num Likes: {{number_format($hashtag->num_likes)}}</li>
			<li>Num Comments: {{number_format($hashtag->num_comments)}}</li>
			<li>{{$hashtag->name}} is used on {{$igposts->total()}} times in the top posts</li>
		</ul>
	</div>
	<div class="pure-u-1-1 pure-u-sm-1-2">
		<h4>Commonly Associated Hashtags</h4>
		@if(isset($associatedTags) && !$associatedTags->isEmpty()) 
		<div class="pure-g associated-tags">
			@foreach($associatedTags as $associatedTag)
				<div class="pure-u-1-1 pure-u-sm-1-2 pure-u-lg-1-3 tag">
					<a href="/hashtags/{{substr($associatedTag->name, 1)}}">{{$associatedTag->name}}</a> <!-- {{str_repeat("#", ceil($associatedTag->pivot->frequency / $associatedTags[0]->pivot->frequency * 5))}} -->
					<div class="pct-bar" style="width:{{100* $associatedTag->pct}}%"></div>
					<div class="pct-bar-back"></div>
				</div>
			@endforeach
		</div>
		@else
			<p>The tag {{$hashtag->name}} does not have associations with other hashtags enough to count in this dataset.</p>
		@endif
	</div>
	<div class="pure-u-1">
	    <h3>Example Posts</h3>
	</div>
    @isset($igposts)
	    @foreach($igposts as $igpost)
	    	<div class="igpost-grid pure-u-1-2 pure-u-sm-1-4 pure-u-lg-1-6">
	    		<div class="pure-g cell">
	    			<!-- <td><a href="/hashtags/{{substr($hashtag->name, 1)}}">{{$hashtag->name}}</a></td> -->
		    		<div class="ig-image pure-u-1">
		    			<a href = "/igposts/{{ $igpost->id }}" ><img src="{{ $igpost->link }}/media/?size=t"></a></br/>
		    		</div>
					<!-- <td>{{ $igpost->link }} </td> -->
					<!-- <td>{{ $igpost->image }} </td> -->
					<!-- <td>{{ $igpost->description }} </td> -->
					<div class="credit pure-u-1">by <a href = "{{ $igpost->link }}">{{ $igpost->user_name }} </a></div>
					<div class="likes pure-u-1-2">♡ ️ {{ $igpost->num_likes }} </div>
					<div class="comments pure-u-1-2">✍ {{ $igpost->num_comments }} </div>
				</div>
	    	</div>
	    @endforeach
	<div class="pure-u-1">
	    {{ $igposts->links() }}
	</div>
    @endisset
@stop
@extends('layouts.base')

@section('title', 'All Hashtags')


@section('content')
    <div class="pure-u-1 pure-u-md-2-3 copy">
        <h1>Hashtags</h1>
        <p>The rules and ecosystem of Instagram make for a unique platform for us photographers; the brevity of posts, heavy use of contextually relevant hashtags, and the required image for a post greatly reduce the noise of most social networks. Obviously when you search for something like #filmisnotdead you - depending on your how exacting your standards may be - may not find the most amazing expressions and technically skillful executions of photographic artistry, but will likely find mostly relevant posts.</p>

    <p>After a while, especially if your interests are narrow to analog photography, you will start to notice patterns in posts and traffic. I thought it would be fun to examine the content in aggregate and see what emerged. My goal was to analyze a representative number of posts over a week that are all related to analog photography. This post addresses some questions of frequency. Represented here are 60k individual posts, 71k hashtags (used 772k times), 28.5k users, 3.6m likes and 145k comments.</p>

    <p>Assuming you are using instagram to connect with others, you will want to use hashtags to make your posts visible to people searching for similar content. Within the analog photography world, how do you connect with others? What hashtags are people using - gear (#mamiya) or lifestyle (#filmisnotdead)? Which version of similar hashtags (#staypoorshootfilm, #staybrokeshootfilm) is the most popular? Explore the most common analog themed tags below!</p>
    </div>
    <div class="pure-u-1 pure-u-md-1-3 copy">
        <strong>Count</strong>
        <p>How frequently this hashtag occurs in the sample dataset. More generic terms tend to rank higher. This is used as a measure of popularity.</p>

        <strong>Likes</strong>
        <p>Sum of all the likes of all the posts for this hashtag. This is a measure of effectiveness.</p>

        <strong>Comments</strong>
        <p>Total count of all the comments written on all the posts for this hashtag. This is a measure of engagement potential.</p>
    </div>
    <div class="pure-u-1">
        <div class="pure-g associated-tags">
            @foreach($hashtags as $hashtag)
                <div class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-3 pure-u-lg-1-4 tag-cell">
                    <div class="tag">
                        <a href="/hashtags/{{substr($hashtag->name, 1)}}">{{$hashtag->name}}</a>
                        <div class="stats pure-g">
                            <div class="count bar-container pure-u-1-3">
                                <div class="stats-bar " style="width:{{floor(100 * $hashtag->count / $hashtags[0]->count)}}%">count</div>
                            </div>
                            <div class="likes bar-container pure-u-1-3">
                                <div class="stats-bar " style="width:{{floor(100 * $hashtag->num_likes / $hashtags[0]->num_likes)}}%">likes</div>
                            </div>
                            <div class="comments bar-container pure-u-1-3">
                                <div class="stats-bar " style="width:{{floor(100 * $hashtag->num_comments / $hashtags[0]->num_comments)}}%">comments</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
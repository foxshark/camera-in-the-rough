@extends('layouts.base')

@section('title', 'About Cameras in the Rough')


@section('content')
<div class="pure-u-1 pure-u-md-1-4">
</div>
<div class="pure-u-1 pure-u-md-1-2">
	<h3>Camera in the Rough</h3>
        <p>The rules and ecosystem of Instagram make for a unique platform for us photographers; the brevity of posts, heavy use of contextually relevant hashtags, and the required image for a post greatly reduce the noise of most social networks. Obviously when you search for something like #filmisnotdead you - depending on your how exacting your standards may be - may not find the most amazing expressions and technically skillful executions of photographic artistry, but will likely find mostly relevant posts.</p>

        <p>After a while, especially if your interests are narrow to analog photography, you will start to notice patterns in posts and traffic. I thought it would be fun to examine the content in aggregate and see what emerged. My goal was to analyze a representative number of posts over a week that are all related to analog photography. This post addresses some questions of frequency. Represented here are 60k individual posts, 71k hashtags (used 772k times), 28.5k users, 3.6m likes and 145k comments.</p>

        <p>Assuming you are using instagram to connect with others, you will want to use hashtags to make your posts visible to people searching for similar content. Within the analog photography world, how do you connect with others? What hashtags are people using - gear (#mamiya) or lifestyle (#filmisnotdead)? Which version of similar hashtags (#staypoorshootfilm, #staybrokeshootfilm) is the most popular?</p>
</div>
<div class="pure-u-1 pure-u-md-1-4">
</div>
@stop
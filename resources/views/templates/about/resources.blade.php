@extends('layouts.base')

@section('title', 'Photography Resources')


@section('content')
<div class="pure-u-1">
	<h3>Photography Related Reviews</h3>
	<ul>
		<li><a target="_blank" href="https://www.pcmag.com/reviews/digital-cameras">PCMag</a></li>
		<li><a target="_blank" href="https://blog.mingthein.com/">Ming Thein</a></li>
	</ul>

	<h3>Analog Photography</h3>
	<ul>
		<li><a target="_blank" href="https://www.digitaltruth.com/devchart.php">Massive Dev Chart</a> Everything you could want to know about time, temp, and dilution</li>
		<li><a target="_blank" href="http://www.toyoview.com/LensSelection/lensselect.html">Toyo Camera</a> lens focal length comparison</li>
	</ul>

	<h3>Photographic Theory</h3>
	<ul>
		<li><a target="_blank" href="http://strobist.blogspot.com/">The Strobist</a> Lighting with small flashes and modifiers</li>
	</ul>

</div>
@stop
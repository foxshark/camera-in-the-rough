@extends('layouts.base')

@section('title', 'All Hashtags')


@section('content')
	<div class="pure-u-1-1 pure-u-sm-1-2">
		<a href = "{{ $igpost->link }}" ><img src="{{ $igpost->link }}/media/?size=m"></a>
		<br/> <a target="_blank" href = "{{ $igpost->link }}">View on Instagram</a>
		<!-- <br/> <a href= "{{ $igpost->link }}/media/?size=l">view full size</a> -->
		<ul>
			<li>Photographer: <a target="_blank" href = "{{ $igpost->link }}">{{ $igpost->user_name }} </a></li>
			<li>Num Likes: {{number_format($igpost->num_likes)}}</li>
			<li>Num Comments: {{number_format($igpost->num_comments)}}</li>
		</ul>
	</div>
    @isset($hashtags)
		<div class="pure-u-1-1 pure-u-sm-1-2">
			<h3>Hashtags Used</h3>
				@isset($hashtags)
					<div class="pure-g associated-tags">
						@foreach($hashtags as $hashtag)
							<div class="pure-u-1-1 pure-u-sm-1-2 pure-u-lg-1-3 tag">
								<a href="/hashtags/{{substr($hashtag->name, 1)}}">{{$hashtag->name}}</a>
							</div>
						@endforeach
					</div>
				@endisset
		</div>
	@endisset
@stop
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIgposts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('igposts', function (Blueprint $table) {
            $table->increments('id');
            $table->char("ig_hash", 64);
            $table->char('link', 255);
            $table->char('image', 255);
            $table->text('description')->nullable();;
            $table->char('user_name', 128);
            $table->dateTime('post_date');
            $table->integer('num_likes');
            $table->integer('num_comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('igposts');
    }
}
